# Copyright (C) 2019 Néfix Estrada
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from telegram import ReplyKeyboardRemove, ParseMode
from telegramcalendar import telegramcalendar
import db as database

db = database.DB()


def list_events(bot, update):
    """
    list_events shows a list with all the events that haven't already passed
    """
    db_events = db.list_events()
    events = ""
    if len(db_events) > 0:
        for event in db_events:
            events += f"*{event[1][:-3]}* - {event[0]}\n"

    else:
        events = "No events yet!"

    update.message.reply_text(events, parse_mode=ParseMode.MARKDOWN)


def list_all_events(bot, update):
    """
    list_all_events shows a list with all the events (also the ones that have passed)
    """
    db_events = db.list_all_events()
    events = ""
    if len(db_events) > 0:
        for event in db_events:
            events += f"*{event[1][:-3]}* - {event[0]}\n"

    else:
        events = "No events yet!"

    update.message.reply_text(events, parse_mode=ParseMode.MARKDOWN)


def help(bot, update):
    """
    help returns a list with all the commands and a small description of what it does
    """
    help_text = """
`/help` --> Shows the bot help
`/new` --> Creates a new event
`/delete` --> Deletes an event
`/check` --> Checks the events of a specific day
`/list` --> Lists the upcoming events
`/list_all` --> Lists all the events
"""

    update.message.reply_text(help_text, parse_mode=ParseMode.MARKDOWN)
