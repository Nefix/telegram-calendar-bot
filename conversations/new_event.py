# Copyright (C) 2019 Néfix Estrada
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from telegram import (
    ReplyKeyboardMarkup,
    ReplyKeyboardRemove,
    InlineKeyboardButton,
    InlineKeyboardMarkup,
)
from telegram.ext import (
    ConversationHandler,
    CallbackQueryHandler,
    CommandHandler,
    MessageHandler,
    Filters,
)
from telegramcalendar import telegramcalendar
import db as database

db = database.DB()

DATE_HANDLER, TIME_HANDLER, TITLE = range(3)


def date_keyboard(bot, update):
    """
    date_keyboard shows a calendar and lets the user to choose a date
    """
    update.message.reply_text(
        "Please, select a date:", reply_markup=telegramcalendar.create_calendar()
    )

    return DATE_HANDLER


def date_handler(bot, update, user_data):
    """
    date_handler is the handler of the calendar
    """
    selected, date = telegramcalendar.process_calendar_selection(bot, update)
    if not selected:
        return DATE_HANDLER

    user_data["date"] = date

    user_data["step"] = "hour"
    noon = "AM"
    user_data["noon"] = noon
    keyboard = hour_keyboard(noon)

    bot.edit_message_text(
        text="Which time is the event?",
        chat_id=update.callback_query.message.chat_id,
        message_id=update.callback_query.message.message_id,
        reply_markup=InlineKeyboardMarkup(keyboard),
    )

    return TIME_HANDLER


def hour_keyboard(noon):
    """
    hour_keyboard returns a hourly clock
    """
    return [
        [
            InlineKeyboardButton("11", callback_data=11),
            InlineKeyboardButton("12", callback_data=12),
            InlineKeyboardButton("1", callback_data=1),
        ],
        [
            InlineKeyboardButton("10", callback_data=10),
            InlineKeyboardButton(" ", callback_data="IGNORE"),
            InlineKeyboardButton("2", callback_data=2),
        ],
        [
            InlineKeyboardButton("9", callback_data=9),
            InlineKeyboardButton(noon, callback_data=noon),
            InlineKeyboardButton("3", callback_data=3),
        ],
        [
            InlineKeyboardButton("8", callback_data=8),
            InlineKeyboardButton(" ", callback_data="IGNORE"),
            InlineKeyboardButton("4", callback_data=4),
        ],
        [
            InlineKeyboardButton("7", callback_data=7),
            InlineKeyboardButton("6", callback_data=6),
            InlineKeyboardButton("5", callback_data=5),
        ],
    ]


def minute_keyboard():
    """
    minute_keyboard returns a small clock with the minues
    """
    return [
        [
            InlineKeyboardButton(" ", callback_data="IGNORE"),
            InlineKeyboardButton("00", callback_data=00),
            InlineKeyboardButton(" ", callback_data="IGNORE"),
        ],
        [
            InlineKeyboardButton("45", callback_data=45),
            InlineKeyboardButton(" ", callback_data="IGNORE"),
            InlineKeyboardButton("15", callback_data=15),
        ],
        [
            InlineKeyboardButton(" ", callback_data="IGNORE"),
            InlineKeyboardButton("30", callback_data=30),
            InlineKeyboardButton(" ", callback_data="IGNORE"),
        ],
    ]


def time_handler(bot, update, user_data):
    """
    time_handler is the handler of the clock
    """
    query = update.callback_query
    data = query.data
    if data == "IGNORE":
        bot.answer_callback_query(callback_query_id=query.id)

        return TIME_HANDLER

    if user_data["step"] == "hour":
        if data == "AM" or data == "PM":
            if data == "AM":
                noon = "PM"

            elif data == "PM":
                noon = "AM"

            user_data["noon"] = noon
            keyboard = hour_keyboard(noon)
            bot.edit_message_text(
                text=query.message.text,
                chat_id=query.message.chat_id,
                message_id=query.message.message_id,
                reply_markup=InlineKeyboardMarkup(keyboard),
            )

            return TIME_HANDLER

        user_data["hour"] = data
        user_data["step"] = "minute"

        bot.edit_message_text(
            text="What's the minute of the event?",
            chat_id=query.message.chat_id,
            message_id=query.message.message_id,
            reply_markup=InlineKeyboardMarkup(minute_keyboard()),
        )

        return TIME_HANDLER

    user_data["minute"] = data

    bot.answer_callback_query(callback_query_id=query.id)
    bot.edit_message_text(
        text="What's the name of the event?",
        chat_id=update.callback_query.message.chat_id,
        message_id=update.callback_query.message.message_id,
    )

    return TITLE


def title(bot, update, user_data):
    """
    title handles the title of the event
    """
    user_data["title"] = update.message.text

    hour = int(user_data["hour"])
    if user_data["noon"] == "PM":
        hour += 12

    date = user_data["date"]
    date = date.replace(hour=hour, minute=int(user_data["minute"]))

    db.add_event(user_data["title"], date.strftime("%Y-%m-%d %H:%M:%S"))
    update.message.reply_text(f"Event {user_data['title']} created at {date}")

    return ConversationHandler.END


def cancel(bot, update):
    """
    cancel cancels the new event creation
    """
    update.message.reply_text(
        "New event cancelled!", reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END


new_event_conv = ConversationHandler(
    entry_points=[CommandHandler("new", date_keyboard)],
    states={
        DATE_HANDLER: [CallbackQueryHandler(date_handler, pass_user_data=True)],
        TIME_HANDLER: [CallbackQueryHandler(time_handler, pass_user_data=True)],
        TITLE: [MessageHandler(Filters.text, title, pass_user_data=True)],
    },
    fallbacks=[CommandHandler("cancel", cancel)],
)
