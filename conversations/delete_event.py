# Copyright (C) 2019 Néfix Estrada
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from telegram import ParseMode
from telegram.ext import ConversationHandler, CommandHandler, MessageHandler, Filters

import db as database

db = database.DB()

ID_HANDLER, CONFIRMATION = range(2)


def list_events(bot, update):
    """
    list_events lists the upcoming events. If no upcoming events are found, the conversation ends
    """
    db_events = db.list_events(return_id=True)
    if len(db_events) > 0:
        events = ""

        for event in db_events:
            events += f"*{event[0]}* | *{event[2][:-3]}* - {event[1]}\n"

        update.message.reply_text(
            "Which event you want to delete? (respond with the event ID)\n\n" + events,
            parse_mode=ParseMode.MARKDOWN,
        )
        return ID_HANDLER

    else:
        update.message.reply_text("No events yet!")
        return ConversationHandler.END


def id_handler(bot, update, user_data):
    """
    id_handler selects the ID of the event that is going to be deleted
    """
    event = db.get_event(update.message.text)
    if event == None:
        update.message.reply_text(
            "The event wasn't found. Make sure you've entered the event ID correctly!"
        )
        return ID_HANDLER

    user_data["event_id"] = event[0]
    update.message.reply_text(
        f"Are you sure you want to delete event #{event[0]} ({event[1]})? Respond with 'YES' to confirm"
    )

    return CONFIRMATION


def confirmation(bot, update, user_data):
    """
    confirmation asks for a deletion confirmation to the user
    """
    if update.message.text.lower() != "yes":
        update.message.reply_text("Event deletion cancelled!")
        return ConversationHandler.END

    db.delete_event(user_data["event_id"])

    update.message.reply_text(f"Event #{user_data['event_id']} successfully deleted!")
    return ConversationHandler.END


def cancel(bot, update):
    """
    cancel cancels the deletion of the event
    """
    update.message.reply_text("Event deletion cancelled!")
    return ConversationHandler.END


delete_event_conv = ConversationHandler(
    entry_points=[CommandHandler("delete", list_events)],
    states={
        ID_HANDLER: [MessageHandler(Filters.text, id_handler, pass_user_data=True)],
        CONFIRMATION: [MessageHandler(Filters.text, confirmation, pass_user_data=True)],
    },
    fallbacks=[CommandHandler("cancel", cancel)],
)
