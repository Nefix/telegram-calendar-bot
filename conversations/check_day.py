# Copyright (C) 2019 Néfix Estrada
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from telegram import ParseMode
from telegram.ext import ConversationHandler, CallbackQueryHandler, CommandHandler
from telegramcalendar import telegramcalendar
import db as database

db = database.DB()

DATE_HANDLER = 0


def date_keyboard(bot, update):
    """
    date_keyboard shows a calendar and lets the user to choose a date
    """
    update.message.reply_text(
        "Please, select a date:", reply_markup=telegramcalendar.create_calendar()
    )

    return DATE_HANDLER


def date_handler(bot, update):
    """
    date_handler handles the event check
    """
    selected, date = telegramcalendar.process_calendar_selection(bot, update)
    if selected:
        db_events = db.list_events_by_day(date.strftime("%Y-%m-%d"))

        if len(db_events) == 0:
            bot.edit_message_text(
                text="There are no events programmed for this day!",
                chat_id=update.callback_query.message.chat_id,
                message_id=update.callback_query.message.message_id,
            )

            return ConversationHandler.END

        events = ""
        for event in db_events:
            events += f"*{event[1][:-3]}* - {event[0]}\n"

        bot.edit_message_text(
            text=events,
            chat_id=update.callback_query.message.chat_id,
            message_id=update.callback_query.message.message_id,
            parse_mode=ParseMode.MARKDOWN,
        )

        return ConversationHandler.END


def cancel(bot, update):
    """
    cancel cancels the event check
    """
    bot.edit_message_text(
        text="Event check cancelled!",
        chat_id=update.callback_query.message.chat_id,
        message_id=update.callback_query.message.message_id,
    )

    return ConversationHandler.END


check_day_conv = ConversationHandler(
    entry_points=[CommandHandler("check", date_keyboard)],
    states={DATE_HANDLER: [CallbackQueryHandler(date_handler)]},
    fallbacks=[CommandHandler("cancel", cancel)],
)
