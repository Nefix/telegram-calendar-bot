# Copyright (C) 2019 Néfix Estrada
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sqlite3

DB_PATH = "/data/calendar.db"


class DB:
    def __init__(self):
        conn = sqlite3.connect(DB_PATH)
        cur = conn.cursor()
        cur.execute(
            """CREATE TABLE IF NOT EXISTS events(
                title TEXT NOT NULL,
                date DATE NOT NULL
            )"""
        )
        conn.commit()
        cur.close()
        conn.close()

    @staticmethod
    def list_events(return_id=False):
        """
        list_events returns an array with all the events that haven't passed yet
        """
        if return_id:
            return_id = "rowid,"
        else:
            return_id = ""

        conn = sqlite3.connect(DB_PATH)
        cur = conn.cursor()
        cur.execute(
            f"""SELECT {return_id} *
               FROM events
               WHERE date >= DATE('now')
               ORDER BY date ASC"""
        )
        events = cur.fetchall()
        cur.close()
        conn.close()

        return events

    @staticmethod
    def list_all_events():
        """
        list_all_events returns an array with all the events (also the ones that have already passed)
        """
        conn = sqlite3.connect(DB_PATH)
        cur = conn.cursor()
        cur.execute(
            """SELECT *
               FROM events
               ORDER BY date ASC"""
        )
        events = cur.fetchall()
        cur.close()
        conn.close()

        return events

    @staticmethod
    def list_events_by_day(day):
        """
        list_events_by_day returns an array with all the events in a specific day
        """
        conn = sqlite3.connect(DB_PATH)
        cur = conn.cursor()
        cur.execute(
            """SELECT *
               FROM events
               WHERE DATE(date) == ?
               ORDER BY date ASC""",
            (day,),
        )
        events = cur.fetchall()
        cur.close()
        conn.close()

        return events

    @staticmethod
    def get_event(event_id):
        """
        get_event returns a specific event by ID. If this event doesn't exist, returns None
        """
        conn = sqlite3.connect(DB_PATH)
        cur = conn.cursor()
        cur.execute(
            f"""SELECT rowid, *
               FROM events
               WHERE rowid = ?""",
            (event_id,),
        )
        event = cur.fetchall()
        cur.close()
        conn.close()

        if len(event) > 0:
            event = event[0]

        else:
            event = None

        return event

    @staticmethod
    def add_event(title, date):
        """
        add_event adds a new event to the DB
        """
        conn = sqlite3.connect(DB_PATH)
        cur = conn.cursor()
        cur.execute("INSERT INTO events VALUES(?, ?)", (title, date))
        conn.commit()
        cur.close()
        conn.close()

    @staticmethod
    def delete_event(event_id):
        """
        delete_event deletes a specific event from the DB
        """
        conn = sqlite3.connect(DB_PATH)
        cur = conn.cursor()
        cur.execute("DELETE FROM events WHERE rowid = ?", (event_id,))
        conn.commit()
        cur.close()
        conn.close()
