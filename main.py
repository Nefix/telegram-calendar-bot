# Copyright (C) 2019 Néfix Estrada
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import json
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler
import db
import commands
from conversations.new_event import new_event_conv
from conversations.delete_event import delete_event_conv
from conversations.check_day import check_day_conv

# Setup logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

# Read the token
with open("/data/token.json") as f:
    TKN = json.load(f)["tkn"]

updater = Updater(token=TKN)
dispatcher = updater.dispatcher

dispatcher.add_handler(new_event_conv)
dispatcher.add_handler(delete_event_conv)
dispatcher.add_handler(check_day_conv)

list_events_handler = CommandHandler("list", commands.list_events)
dispatcher.add_handler(list_events_handler)

list_all_events_handler = CommandHandler("list_all", commands.list_all_events)
dispatcher.add_handler(list_all_events_handler)

help_handler = CommandHandler("help", commands.help)
dispatcher.add_handler(help_handler)

updater.start_polling()
updater.idle()
